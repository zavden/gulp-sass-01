const { src, dest, watch, series, parallel } = require("gulp");
const sass = require("gulp-sass")(require('sass'));
const postcss = require("gulp-postcss");
const autoprefixer = require("autoprefixer");
const imagemin = require("gulp-imagemin");
const webp = require("gulp-webp");
const avif = require("gulp-avif");

function css(done){
  // Compilar SASS
  // 1. Identificar archivo
  src("src/scss/app.scss")
  // 2. Compilar
    .pipe(sass(
      // { outputStyle: 'compressed'}
    ))
    .pipe( postcss( [ autoprefixer() ] ))
  // 3. Guardar el .css, gulo creara la carpeta "build"
  // en caso de que no exita
    .pipe( dest('build/css') );
  // 4. Indicar que el proceso ha sido completado
  done();
}

function toWebp() {
  return src("src/img/**/*.{png,jpg,jpeg}")
    .pipe(webp())
    .pipe(dest("build/img"))
}

function toAvif() {
  return src("src/img/**/*.{png,jpg,jpeg}")
    .pipe(avif({
      quality: 50
    }))
    .pipe(dest("build/img"))
}

// Watchers
function watchsass(){
  watch('src/scss/**/*.scss', css);
  watch('src/img/**/*', css);
}

function images(done) {
  src("src/img/**/*")
    .pipe( imagemin({
      optimizationLevel: 3
    }) )
    .pipe( dest("build/img") );
  done();
}

exports.css = css;
exports.dev = watchsass;
exports.images = images;
exports.toWebp = toWebp;
exports.toAvif = toAvif;
exports.buildImages = series(images, toWebp, toAvif)
exports.build = series(images, toWebp, toAvif, css, watchsass)
exports.default = series( css, watchsass );
